var express = require('express');
var mongoose = require('mongoose');

exports.api = express.Router();

var commentSchema = new mongoose.Schema({
    body: String,
    date: Date
});

var productSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        min: 0,
        max: 999999,
        required: true,
        default: 0
    },
    created: {
        type: Date,
        default: Date.now
    },
    comments: [commentSchema]
});

var Product = mongoose.model('Product', productSchema);

exports.api.route('/new')
.post(function (request, response, next) {
    var product = new Product(request.body);
    product.save(function (error, product) {
        if (error)
            return next(error);
        response.json(product);
    });
});

exports.api.route('/all')
.get(function (request, response, next) {
    Product.find(function (error, products) {
        if (error)
            return next(error);
        response.json(products);
    });
});

exports.api.route('/comment')
.post(function (request, response, next) {
    Product.findByIdAndUpdate(request.body.id, {
        $push: { 
            comments: { 
                body: request.body.comment
            } 
        }
    }, {
        new: true
    }, function (error, product) {
        if (error)
            return next(error);

        response.json(product);
    });
});
